# SPDX-FileCopyrightText: 2019-2023 Blender Foundation
#
# SPDX-License-Identifier: GPL-3.0-or-later

# --------------------------------------------------------------------------
# The sun positioning algorithms are based on the National Oceanic and
# Atmospheric Administration's (NOAA) Solar Calculator which relies on
# calculations from Jean Meeus' book "Astronomical Algorithms."
# Use of NOAA data and products are in the public domain and may be used freely
# by the public as outlined in their policies at:
# https://gml.noaa.gov/about/disclaimer.html
# --------------------------------------------------------------------------
# The geo parser script is by Maximilian Högner, released
# under the GNU GPL license:
# https://hoegners.de/Maxi/geo/
# --------------------------------------------------------------------------


if "bpy" in locals():
    import importlib
    importlib.reload(properties)
    importlib.reload(ui_sun)
    importlib.reload(environment)
    importlib.reload(sun_calc)
    importlib.reload(draw)
    importlib.reload(translations)

else:
    from . import properties, ui_sun, environment, sun_calc, draw, translations

import bpy
from bpy.app.handlers import persistent


register_classes, unregister_classes = bpy.utils.register_classes_factory(
    (properties.SunPosProperties,
     properties.SunPosAddonPreferences, ui_sun.SUNPOS_OT_AddPreset,
     ui_sun.SUNPOS_PT_Presets, ui_sun.SUNPOS_PT_Panel,
     ui_sun.SUNPOS_PT_Location, ui_sun.SUNPOS_PT_Time, environment.SUNPOS_OT_ShowEnvironment))


@persistent
def sun_frame_handler(scene):
    sun_props = scene.sun_pos_properties
    if sun_props.usage_mode == "ENVIRONMENT":
        sun_calc.move_sun_env(scene)
    else:
        sun_calc.move_sun(scene)

    if not (bpy.app.is_job_running("RENDER") or bpy.app.is_job_running("OBJECT_BAKE")):
        if sun_props.show_surface or sun_props.show_analemmas:
            draw.analemmas_surface_update(scene)
        if sun_props.show_north:
            draw.north_update(scene)


@persistent
def sun_scene_handler(_filename):
    """Update drawing on file load."""
    scene = bpy.context.scene
    draw.analemmas_surface_update(scene)
    draw.north_update(scene)


def register():
    register_classes()
    bpy.types.Scene.sun_pos_properties = (
        bpy.props.PointerProperty(type=properties.SunPosProperties,
                                  name="Sun Position",
                                  description="Sun Position Settings"))
    bpy.app.handlers.frame_change_pre.append(sun_frame_handler)
    bpy.app.handlers.load_post.append(sun_scene_handler)
    bpy.app.translations.register(__name__, translations.translations_dict)


def unregister():
    bpy.app.translations.unregister(__name__)
    bpy.app.handlers.frame_change_pre.remove(sun_frame_handler)
    bpy.app.handlers.load_post.remove(sun_scene_handler)
    del bpy.types.Scene.sun_pos_properties
    unregister_classes()
